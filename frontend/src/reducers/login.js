import { LOGIN, LOGIN_SUCCESS, LOGIN_ERROR, LOGOUT } from '../redux/types'

export default ( state = {
  auth: false,
  token: null,
  isLoading: false,
  error: ''
}, action ) => {
  switch(action.type){
    case LOGIN:
      return {...state, isLoading: true }
    case LOGIN_SUCCESS:
      return { ...state, isLoading: false, ...action.payload }
    case LOGIN_ERROR:
      return { ...state, isLoading: false, error: 'Invalid credentials'}
    case LOGOUT: 
      return null
    default: 
      return state
  }
}