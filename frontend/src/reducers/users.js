import { 
  GET_USERS, 
  GET_USERS_SUCCESS, 
  GET_USERS_ERROR,
  CREATE_USER, 
  CREATE_USER_SUCCESS, 
  CREATE_USER_ERROR,
  GET_USER, 
  GET_USER_SUCCESS, 
  GET_USER_ERROR
} from '../redux/types'

export default (
  state = {
    data: [],
    isLoading: false,
    error: ''
  }
, action) => {
  switch(action.type){
    case GET_USERS:
    case CREATE_USER:
    case GET_USER:   
      return { ...state, isLoading: true }
    case GET_USERS_SUCCESS:
      return { ...state, data: [...action.payload], isLoading: false }
    case GET_USERS_ERROR:
      return { ...state, error: 'Error fetching users', isLoading: false }
      case CREATE_USER_SUCCESS:
        state.data.push(action.payload)
        return { data: [...state.data], isLoading: false, error: '' }
    case CREATE_USER_ERROR:
      return { ...state, error: 'Error creating user', isLoading: false }
    case GET_USER_SUCCESS: 
      return { ...state, data: action.payload, isLoading: false }
    case GET_USER_ERROR: 
      return { ...state, error: 'Error getting user details', isLoading: false }

    default: 
      return state
  }
}