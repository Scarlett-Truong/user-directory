import { combineReducers } from "redux";
import auth from './login';
import users from './users';

export default combineReducers({
  auth,
  users
});