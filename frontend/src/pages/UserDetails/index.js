import React from 'react';
import UserDetails from '../../components/UserDetails'

export default (props) => {
  return (
    <UserDetails userId={props.match.params.id}/>
  )
}
