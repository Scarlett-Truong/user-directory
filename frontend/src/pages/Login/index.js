import React, { useState } from 'react'
import { TextField, Container, CssBaseline, Avatar, Typography, Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import {useForm} from "react-hook-form"
import axios from 'axios'
import { SERVER_URL } from '../../config/constants'
import history from '../../helpers/history'
import Snackbar from '@material-ui/core/Snackbar'
import MuiAlert from '@material-ui/lab/Alert'

const useStyles = makeStyles(theme => ({
  pape: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function LogIn() {
  const classes = useStyles()
  const { register, handleSubmit, errors } = useForm()
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [errorAlert, setErrorAlert] = useState(false)
  const vertical = 'top'
  const horizontal = 'center'

  const handleFormSubmit = () => {
    login()
  }

  const handleCloseAlert = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setErrorAlert(false)
  }

  const login = () => {
    axios.post(`${SERVER_URL}/login`, {
      email: email,
      password: password
    })
    .then(res => {
      if(res.status === 200){
        localStorage.setItem('token', res.data.token)
        setErrorAlert(false)
        history.push('./users')
      }
    })
    .catch(err => {
      setErrorAlert(true)
    })
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.pape}>
        <Avatar className={classes.avatar}></Avatar>
        <Typography component="h1" variant="h5">User Directory</Typography>
        <form className={classes.form} noValidate onSubmit={handleSubmit(handleFormSubmit)}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={e => setEmail(e.target.value)}
            error={!!errors.email}
            inputRef={register({
              required: true,
              pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            })}
            helperText={
              (errors.email)? 
              (errors.email.type === 'pattern'? "Invalid email address" : "Required")
              : ""
            }
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={e => setPassword(e.target.value)}
            error={!!errors.password}
            inputRef={register({ required: true })}
            helperText={!!errors.password && "Required"}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Login
          </Button>
        </form>
        <Snackbar open={errorAlert} 
          onClose={handleCloseAlert}
          anchorOrigin={{ vertical, horizontal }}
          key={`${vertical},${horizontal}`}
        >
          <Alert onClose={handleCloseAlert} severity="error">
            Something wrong :(
          </Alert>
        </Snackbar>
      </div>
    </Container>
  );
}

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}