import React, { useState, useEffect } from 'react'
import Paper from '@material-ui/core/Paper'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TablePagination from '@material-ui/core/TablePagination'
import TableRow from '@material-ui/core/TableRow'
import { makeStyles } from '@material-ui/core/styles'
import { useDispatch, useSelector } from "react-redux"
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import { logout } from '../../redux/actions'
import SearchAppBar from '../../components/SearchBar'
import axios from 'axios'
import { SERVER_URL } from '../../config/constants'
import history from '../../helpers/history'

const columns = [
  { id: 'id', label: 'ID', minWidth: 20 },
  { id: 'firstName', label: 'First Name', minWidth: 50 },
  { id: 'lastName', label: 'Last Name', minWidth: 50 },
  { id: 'email', label: 'Email', minWidth: 80 },
  { id: 'phone', label: 'Phone', minWidth: 50 },
  { id: 'title', label: 'Title', minWidth: 100 },
];

const useStyles = makeStyles(theme => 
  ({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 600,
    },
    button: {
      width: 200,
      margin: theme.spacing(3, 0, 2)
    }
  })
)

export default function Home() {
  const classes = useStyles()
  const [page, setPage] = useState(0)
  const [rowsPerPage, setRowsPerPage] = useState(10)
  const dispatch = useDispatch()
  const usersFromLogin = useSelector(state => {
    return state.users.data
  })
  const [filteredUsers, setFilteredUsers] = useState([])
  const [filteredName, setFilteredName] = useState(null)
  const [currentUsers, setCurrentUsers] = useState([])
  const [pageLoading, setPageLoading] = useState(true)
  const users = (currentUsers.length > usersFromLogin.length)? currentUsers : usersFromLogin
  const token = localStorage.getItem("token")
  
  useEffect(()=> {
    getCurrentUsers(token)
  }, [])
  
  const getCurrentUsers= (token) => {
    axios.get(`${SERVER_URL}/users`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
    .then((res) => {
      setCurrentUsers(res.data)
      setPageLoading(false)
    })
  }

  const handleChangePage = (event, newPage) => {
    setPage(newPage)
  }

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  }

  const handleSearchChange = event => {
    const lowercasedFilter = event.target.value.toLowerCase()
    setFilteredName(lowercasedFilter)
    const filtered = users.filter(user => {
      return Object.keys(user).some(key => {
        if(key === 'firstName' || key === 'lastName'){
          if(user[key].toString().toLowerCase().includes(lowercasedFilter) && lowercasedFilter !== null) {
            return user[key];
          }
        }
        return false
      });
    })
    setFilteredUsers(filtered)
  }

  const usersData =  (filteredUsers && filteredUsers.length> 0)? filteredUsers : users
  
  return (
    <>
      {pageLoading && <CircularProgress />}
      {(users && users.length > 0) &&
        <>
          <SearchAppBar handleChange={handleSearchChange} />
          <Paper className={classes.root}>
            <TableContainer className={classes.container}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead>
                  <TableRow>
                    {columns.map(column => (
                      <TableCell
                        key={column.id}
                        align={column.align}
                        style={{ minWidth: column.minWidth }}
                      >
                        {column.label}
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {filteredName && filteredUsers.length === 0 ?  
                    <TableRow>
                      <TableCell key='email'>No matching users</TableCell>
                    </TableRow>
                    : 
                    <>
                    {usersData.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(row => {
                      return (
                        <TableRow 
                          hover
                          tabIndex={-1} 
                          key={row.id} 
                          onClick={() => {
                            history.push(`./users/${row.id}`)
                          }}>
                          {columns.map(column => {
                            const value = row[column.id];
                            return (
                              <TableCell key={column.id} align={column.align}>
                                {value}
                              </TableCell>
                            );
                          })}
                        </TableRow>
                      );
                    })}
                    </>
                  }
                </TableBody>
              </Table>
            </TableContainer>
            <TablePagination
              rowsPerPageOptions={[10, 25, 100]}
              component="div"
              count={users.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={handleChangePage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </Paper>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={() => {
              dispatch(logout())
            }}
          >
            Logout
          </Button>
        </>
        // : <CircularProgress />
      }
    </>
  );
}
