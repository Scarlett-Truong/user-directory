import React from 'react'
import { Router, Route, Switch } from 'react-router-dom';
import Home from './pages/Home'
import Login from './pages/Login'
import UserDetails from './pages/UserDetails'
import './App.scss'
import requiresAuth from './helpers/requiresAuth'
import { connect } from 'react-redux'
import history from './helpers/history'

class App extends React.Component {
  render(){
    return (
      <Router history={history}>
          <div className="appBody">
            <Switch>
              <Route exact path='/users' component={requiresAuth(Home)}/>
              <Route exact path='/login' component={Login}/>
              <Route exact path='/users/:id' component={requiresAuth(UserDetails)}/>
              <Route path='/' component={Login}/>
            </Switch>
          </div>
      </Router>
    )
  }
}

const mapStateToProps = ({ auth, users }) => {
  return {
    auth,
    users,
  };
};

export default connect(
  mapStateToProps,
  {}
)(App);

