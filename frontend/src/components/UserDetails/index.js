import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { Grid, Button, Container } from '@material-ui/core'
import history from '../../helpers/history'
import axios from 'axios'
import { SERVER_URL } from '../../config/constants'
import { useForm } from "react-hook-form"
import { green } from '@material-ui/core/colors';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

const useStyles = makeStyles(theme => ({
  page: {
    marginTop: theme.spacing(15),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  button: {
    marginTop: theme.spacing(3)
  },
  fabProgress: {
    color: green[500],
    zIndex: 1,
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

export default function UserDetails(props) {
  const classes = useStyles();
  const [userInfo, setUserInfo] = useState({})
  const [changedInfo, setChangedInfo] = useState({})
  const [success, setSuccess] = useState(false)
  const token = localStorage.getItem("token")
  const { register, handleSubmit, errors } = useForm()
  const vertical = 'top'
  const horizontal = 'center'

  useEffect(() => {
    getUserDetails(token, props.userId)
  },[])

  const getUserDetails = (token, userId) => {
    axios.get(`${SERVER_URL}/users/${userId}`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    })
    .then((res) => {
      setUserInfo(res.data)
    })
    .catch(err=> {
      console.log(err)
    })
  }

  const handleFormChange = (event) => {
    const { id, value } = event.target;
    setChangedInfo(prevState => ({ ...prevState, [id]: value }));
  }
  
  const updateUser = () => {
    if(Object.keys(changedInfo).length > 0){
      axios.patch(`${SERVER_URL}/users/${props.userId}`,changedInfo, {
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        if(res.status === 200){
          setUserInfo(res.data)
          setSuccess(true)
        }
      })
      .catch(err => {
        setSuccess(false)
      })
    }
  }

  const handleCloseAlert = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setSuccess(false)
  };

  return (
    <Container component="main" maxWidth="sm">
      <div className={classes.page}>
        <form className="" 
          noValidate
          onSubmit={handleSubmit(updateUser)}
        >
          {Object.keys(userInfo).length > 0 &&
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <TextField
                  autoFocus
                  margin="dense"
                  id="firstName"
                  name="firstName"
                  label="First name"
                  fullWidth
                  defaultValue={userInfo.firstName}
                  onChange={handleFormChange}
                  error={!!errors.firstName}
                  inputRef={register({ required: true })}
                  helperText={errors.firstName && "Required"}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  margin="dense"
                  id="lastName"
                  name="lastName"
                  label="Last name"
                  fullWidth
                  defaultValue={userInfo.lastName}
                  onChange={handleFormChange}
                  error={!!errors.lastName}
                  inputRef={register({ required: true })}
                  helperText={!!errors.lastName && "Required"}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  margin="dense"
                  id="email"
                  name="email"
                  label="Email Address"
                  type="email"
                  fullWidth
                  defaultValue={userInfo.email}
                  onChange={handleFormChange}
                  error={!!errors.email}
                  inputRef={register({
                    required: true,
                    pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                  })}
                  helperText={
                    (errors.email)? 
                    (errors.email.type === 'pattern'? "Invalid email address" : "Required")
                    : ""
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  margin="dense"
                  id="phone"
                  name="phone"
                  label="Phone"
                  fullWidth
                  defaultValue={userInfo.phone}
                  onChange={handleFormChange}
                  error={!!errors.phone}
                  inputRef={register({ required: true })}
                  helperText={!!errors.phone && "Required"}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  margin="dense"
                  id="title"
                  name="title"
                  label="Title"
                  fullWidth
                  defaultValue={userInfo.title}
                  onChange={handleFormChange}
                />
              </Grid>
              <Grid item xs={6}>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.button}
                  onClick={() => {
                    history.push('../users')
                  }}
                >
                  Cancel
                </Button>
              </Grid>
              <Grid item xs={6}>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.button}
                >
                  Update
                </Button>
              </Grid>
            </Grid>
          }
        </form>
        <Snackbar open={success} 
          onClose={handleCloseAlert}
          anchorOrigin={{ vertical, horizontal }}
          key={`${vertical},${horizontal}`}
        >
          <Alert onClose={handleCloseAlert} severity="success">
            Update success!!!
          </Alert>
        </Snackbar>
      </div>
    </Container>
  );
}

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}