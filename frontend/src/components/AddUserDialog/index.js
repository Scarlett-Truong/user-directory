import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button'
import DialogTitle from '@material-ui/core/DialogTitle'
import Dialog from '@material-ui/core/Dialog'
import { blue } from '@material-ui/core/colors'
import TextField from '@material-ui/core/TextField'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import { Grid } from '@material-ui/core'
import {useForm} from "react-hook-form"
import axios from 'axios'
import { SERVER_URL } from '../../config/constants'
import Snackbar from '@material-ui/core/Snackbar'
import MuiAlert from '@material-ui/lab/Alert'

const useStyles = makeStyles({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
});

export default function SimpleDialog(props) {
  const { register, handleSubmit, errors } = useForm()
  const classes = useStyles()
  const token = localStorage.getItem('token')
  const { onClose, open } = props
  const [userInfo, setUserInfo] = useState({
    firstName: '',
    lastName: '',
    password: '',
    email: '',
    phone: '',
    title: ''
  })
  const [success, setSuccess] = useState(false)
  const vertical = 'top'
  const horizontal = 'center'

  const handleFormChange = (event) => {
    const { id, value } = event.target;
    setUserInfo(prevState => ({ ...prevState, [id]: value }));
  }

  const handleFormSubmit = () => {
    if(Object.keys(errors).length === 0){
      createUser(token, userInfo)
    }
  }

  const createUser = (token, userInfo) => {
    axios.post(`${SERVER_URL}/users`, userInfo, 
    {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      }
    })
    .then((res) => {
      if(res.status === 200){
        setSuccess(true)
      }
    })
    .catch(err => {
      setSuccess(false)
    })
  }

  const handleCloseAlert = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setSuccess(false)
    window.location.reload()
  }

  return (
    <>
      <Dialog aria-labelledby="form-dialog-title" open={open}>
        <DialogTitle id="form-dialog-title">Add user account</DialogTitle>
        <form className={classes.form} noValidate onSubmit={handleSubmit(handleFormSubmit)}>
        <DialogContent>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <TextField
                  autoFocus
                  required
                  margin="dense"
                  id="firstName"
                  name="firstName"
                  label="First name"
                  fullWidth
                  onChange={handleFormChange}
                  error={!!errors.firstName}
                  inputRef={register({ required: true })}
                  helperText={errors.firstName && "Required"}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  required
                  margin="dense"
                  id="lastName"
                  name="lastName"
                  label="Last name"
                  fullWidth
                  onChange={handleFormChange}
                  error={!!errors.lastName}
                  inputRef={register({ required: true })}
                  helperText={!!errors.lastName && "Required"}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  margin="dense"
                  id="email"
                  name="email"
                  label="Email Address"
                  type="email"
                  autoComplete="current-email"
                  fullWidth
                  onChange={handleFormChange}
                  error={!!errors.email}
                  inputRef={register({
                    required: true,
                    pattern: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                  })}
                  helperText={
                    (errors.email)? 
                    (errors.email.type === 'pattern'? "Invalid email address" : "Required")
                    : ""
                  }
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  required
                  margin="dense"
                  id="password"
                  name="password"
                  label="Password"
                  type="password"
                  autoComplete="current-password"
                  fullWidth
                  onChange={handleFormChange}
                  error={!!errors.password}
                  inputRef={register({ required: true })}
                  helperText={!!errors.password && "Required"}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  required
                  margin="dense"
                  id="phone"
                  name="phone"
                  label="Phone"
                  fullWidth
                  onChange={handleFormChange}
                  error={!!errors.phone}
                  inputRef={register({ required: true })}
                  helperText={!!errors.phone && "Required"}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  margin="dense"
                  id="title"
                  label="Title"
                  fullWidth
                  onChange={handleFormChange}
                />
              </Grid>
            </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} color="primary">
            Cancel
          </Button>
          <Button type="submit" color="primary">
            Add
          </Button>
        </DialogActions>
        </form>
        <Snackbar open={success} 
          onClose={handleCloseAlert}
          anchorOrigin={{ vertical, horizontal }}
          key={`${vertical},${horizontal}`}
        >
          <Alert onClose={handleCloseAlert} severity="success">
            Add user success!!!
          </Alert>
        </Snackbar>
      </Dialog>
    </>
  );
}

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
}
