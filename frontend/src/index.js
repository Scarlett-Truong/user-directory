import React from 'react'
import ReactDOM from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import reduxThunk from 'redux-thunk'
import reducers from './reducers'
import './index.css'
import App from './App'
const initialState = {
  auth: {
    auth: false,
    token: '',
    isLoading: false,
    error: ''
  },
  users: {
    data: [],
    isLoading: false,
    error: ''
  }
}
const store = createStore(reducers, initialState , applyMiddleware(reduxThunk));

ReactDOM.render(  
  <Provider store={store}>
    <App />
  </Provider>, 
  document.getElementById('root')
);

