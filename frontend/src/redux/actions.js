import axios from 'axios'
import { SERVER_URL } from '../config/constants'
import history from '../helpers/history'
import * as type from './types'

export const getUsersList2 = (token) => (dispatch) => {
  console.log(token)
  // dispatch({ type: type.GET_USERS })
  axios.get(`${SERVER_URL}/users`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
  .then((res) => {
    console.log(res.data)
    dispatch({
      type: type.GET_USERS_SUCCESS,
      payload: res.data
    });
  })
  .catch((err) => {
    dispatch({
      type: type.GET_USERS_ERROR,
      payload: err
    });
  });
}

export const getUsersList = (token, dispatch) => {
  dispatch({
    type: type.GET_USERS,
    // payload: null
    // payload: {
    //   data: [],
    //   isLoading: true
    // }
  });
  return axios.get(`${SERVER_URL}/users`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    },
  })
  .then((res) => {
    dispatch({
      type: type.GET_USERS_SUCCESS,
      payload: res.data
      // payload: {
      //   data: res.data,
      //   isLoading: false
      // }
    });
  })
  .catch((err) => {
    dispatch({
      type: type.GET_USERS_ERROR,
      payload: err
      // payload: {
      //   data: [],
      //   isLoading: false
      // }
    });
  });
}

export const login = (email, password) => (dispatch) => {
  dispatch({ type: type.LOGIN })
  axios.post(`${SERVER_URL}/login`, {
    email: email,
    password: password
  })
  .then(res => {
    if(res.status === 200){
      localStorage.setItem('token', res.data.token)
      dispatch({
        type: type.LOGIN_SUCCESS,
        payload: {
          auth: true,
          token: res.data.token
        }
      })
      // getUsersList2(res.data.token)
      getUsersList(res.data.token, dispatch)
      history.push('./users')
    }
  })
  .catch(err => {
    dispatch({ type: type.LOGIN_ERROR })
  })
}

export const logout = () => (dispatch) => {
  localStorage.removeItem('token')
  dispatch({ type: type.LOGOUT })
  history.push('/login')
}

export const createUser = (token, userInfo)=> dispatch => {
  dispatch({
    type: type.CREATE_USER,
    // payload: null,
    // payload: {
    //   data: {},
    //   isLoading: true
    // }
  })
  axios.post(`${SERVER_URL}/users`, userInfo, 
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    }
  })
  .then((res) => {
    if(res.status === 200){
      dispatch({
        type: type.CREATE_USER_SUCCESS,
        payload: res.data
        // payload: {
        //   data: res.data,
        //   isLoading: false
        // }
      })
      return res.data
    }
  })
  .catch((err) => {
    dispatch({
      type: type.CREATE_USER_ERROR,
      // payload: err
      // payload: {
      //   data: {},
      //   isLoading: false,
      //   error: 'Server error'
      // }
    })
  });
}

export const getUser = (token, userId)=> dispatch => {
  dispatch({ type: type.GET_USER })
  axios.get(`${SERVER_URL}/users/${userId}`,
  {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${token}`,
    }
  })
  .then((res) => {
    if(res.status === 200){
      dispatch({
        type: type.GET_USER_SUCCESS,
        payload: res.data
      })
      return res.data
    }
  })
  .catch((err) => {
    dispatch({
      type: type.GET_USER_ERROR,
    })
  });
}