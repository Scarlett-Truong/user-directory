import React from "react"
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

export default function(ProtectedComponent) {
  class Authenticate extends React.Component {
    render() {
      let display
      const token = localStorage.getItem('token')
      if(!token){
        display = <Redirect to="/login" />
      }
      else {
        display = <ProtectedComponent {...this.props} />
      }
      return (
        <div>
          {display}
        </div>
      );
    }
  }

  const mapStateToProps = ({auth}) => {
    return {
      auth
    };
  };
  return connect(mapStateToProps)(Authenticate);
}
