const { Sequelize } = require('sequelize')
const defineUser = require('./models/define-user')

module.exports = function db (app) {
  // Init DB
  const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: 'data/db.sqlite',
    logging: false
  })
  app.set('sequelize', sequelize)

  // Define models
  const User = defineUser(sequelize)
  app.set('User', User)

  // Sync
  return sequelize.sync()
}
