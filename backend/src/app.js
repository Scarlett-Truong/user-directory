const express = require('express')
const bodyParser = require("body-parser")
const cors = require('cors')
const db = require('./db')
const init = require('./init')
const routes = require('./routes')

module.exports = async function App () {
  const app = express().use('*', cors())
  
  // App setup
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(bodyParser.json())

  // Setup services
  await db(app)
  await init(app)
  await routes(app)
  
  return app
}